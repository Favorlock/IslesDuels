package com.favorlock.duels.tasks;

import com.favorlock.duels.system.DuelState;
import com.favorlock.duels.utils.MessageUtil;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class StartDuel extends BukkitRunnable {

    private DuelState state;
    private Player challenger;
    private Player challenged;
    private int count = 5;

    public StartDuel(DuelState state, Player challenger, Player challenged) {
        this.state = state;
        this.challenger = challenger;
        this.challenged = challenged;
    }

    @Override
    public void run() {
        if (!this.challenger.isOnline() || !this.challenged.isOnline()) {
            cancel();
            duelCancelled(this.challenger.isOnline() ? this.challenger : this.challenged);
        } else if (count <= 0) {
            state.setStarted(true);
            cancel();
        }

        tick(this.challenger);
        tick(this.challenged);
        count--;
    }

    private void duelCancelled(Player player) {
        MessageUtil.send(player, new StringBuilder("&6")
                .append("The duel was cancelled because the other player left.")
                .toString());
    }

    private void tick(Player player) {
        if (count <= 0) {
            MessageUtil.send(player, new StringBuilder("&6")
                    .append("Fight!")
                    .toString());
        } else {
            MessageUtil.send(player, new StringBuilder("&7")
                    .append("Duel starts in ")
                    .append("&6")
                    .append(count)
                    .toString());
        }
    }

}
