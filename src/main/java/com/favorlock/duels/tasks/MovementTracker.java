package com.favorlock.duels.tasks;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MovementTracker implements Runnable, Listener {

    public static final long THRESHOLD = 5L;
    public static final long THRESHOLD_MILLIS = THRESHOLD * 50;

    private Map<UUID, Long> moving = new ConcurrentHashMap<>();

    @Override
    public void run() {
        for (Map.Entry<UUID, Long> entry : this.moving.entrySet()) {
            if (System.currentTimeMillis() - entry.getValue() > THRESHOLD_MILLIS) {
                this.moving.remove(entry.getKey());
            }
        }
    }

    public Map<UUID, Long> getMoving() {
        return new HashMap<>(this.moving);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getFrom().getX() != event.getTo().getX()
                || event.getFrom().getY() != event.getTo().getY()
                || event.getFrom().getZ() != event.getTo().getZ()) {
            this.moving.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
        }
    }

}
