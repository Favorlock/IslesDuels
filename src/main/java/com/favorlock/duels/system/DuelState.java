package com.favorlock.duels.system;

import java.util.UUID;

public class DuelState {

    private UUID challenger;
    private UUID challenged;

    private boolean inProgress = false;
    private boolean started = false;

    public DuelState(UUID challenger, UUID challenged) {
        this.challenger = challenger;
        this.challenged = challenged;
    }

    public UUID getChallenger() {
        return challenger;
    }

    public UUID getChallenged() {
        return challenged;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
