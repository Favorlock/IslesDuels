package com.favorlock.duels.system;

import com.favorlock.duels.Main;
import com.favorlock.duels.tasks.StartDuel;
import com.favorlock.duels.utils.MessageUtil;
import com.favorlock.duels.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class DuelManager implements Listener {

    private static final long MESSAGE_DELAY = 20L;

    private Main plugin;
    private List<DuelState> states = new CopyOnWriteArrayList<>();

    public DuelManager(Main plugin) {
        this.plugin = plugin;
    }

    /**
     * Checks if a player with the provided uuid is
     * currently dueling.
     *
     * @param target player uuid
     * @return true if player is dueling, otherwise false
     */
    public boolean isDueling(UUID target) {
        return this.states.stream().anyMatch(state ->
                (state.getChallenged().equals(target)
                        || state.getChallenger().equals(target))
                        && state.isInProgress());
    }

    /**
     * Returns an optional of an available duel that has yet
     * to start.
     *
     * @param target player uuid
     * @return Optional wrapped DuelState
     */
    public Optional<DuelState> findAvailableDuel(UUID target) {
        return this.states.stream().filter(state ->
                state.getChallenged().equals(target)
                        && !isDueling(state.getChallenger())
                        && !state.isInProgress())
                .findFirst();
    }

    /**
     * Returns an active duel that is set to in progress.
     *
     * @param target player uuid
     * @return Optional wrapped DuelState
     */
    public Optional<DuelState> findActiveDuel(UUID target) {
        return this.states.stream().filter(state ->
                (state.getChallenged().equals(target)
                        || state.getChallenger().equals(target))
                        && state.isInProgress())
                .findFirst();
    }

    /**
     * Accepts a challenge if one is available.
     *
     * @param challenged the player challenged to a duel
     */
    public void accept(Player challenged) {
        if (isDueling(challenged.getUniqueId())) {
            MessageUtil.send(challenged, new StringBuilder("&c")
                    .append("You cannot accept a duel while dueling.")
                    .toString());
        } else {
            Optional<DuelState> optionalState = findAvailableDuel(challenged.getUniqueId());
            if (optionalState.isPresent()) {
                DuelState state = optionalState.get();
                Optional<Player> optionalChallenger = PlayerUtil.find(state.getChallenger());
                if (optionalChallenger.isPresent()) {
                    Player challenger = optionalChallenger.get();
                    state.setInProgress(true);
                    MessageUtil.send(challenger, new StringBuilder("&7")
                            .append("Duel with ")
                            .append("&6")
                            .append(challenged.getName())
                            .append("&7")
                            .append(" will commence momentarily. Teleporting opponent to your position.")
                            .toString());
                    MessageUtil.send(challenged, new StringBuilder("&7")
                            .append("Duel with ")
                            .append("&6")
                            .append(challenger.getName())
                            .append("&7")
                            .append(" will commence momentarily. Teleporting you to opponents position.")
                            .toString());
                    preparePlayer(challenger, challenged, false);
                    preparePlayer(challenged, challenger, true);

                    new StartDuel(state, challenger, challenged).runTaskTimer(this.plugin,20L, 20L);
                }
            }
        }
    }

    /**
     * Invites a player to a duel.
     *
     * @param challenger the challenging player
     * @param challenged the challenged player
     */
    public void invite(Player challenger, Player challenged) {
        if (this.states.stream().anyMatch(state -> state.getChallenger().equals(challenger.getUniqueId())
                && state.getChallenged().equals(challenged.getUniqueId()))) {
            MessageUtil.send(challenger, new StringBuilder("&c")
                    .append("You have already challenged ")
                    .append("&6")
                    .append(challenged.getName())
                    .append("&c")
                    .append(" to a duel.")
                    .toString());
        } else {
            if (this.plugin.getMovementTracker().getMoving().containsKey(challenged.getUniqueId())) {
                MessageUtil.send(challenger, new StringBuilder("&c")
                        .append("Players must be idle for you to challenge them to a duel.")
                        .toString());
            } else {
                DuelState state = new DuelState(challenger.getUniqueId(), challenged.getUniqueId());
                this.states.add(state);
                Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
                    Optional<Player> optionalSender = PlayerUtil.find(state.getChallenger());
                    Optional<Player> optionalTarget = PlayerUtil.find(state.getChallenged());
                    if (optionalSender.isPresent() && optionalTarget.isPresent()) {
                        Player sender = optionalSender.get();
                        Player target = optionalTarget.get();
                        MessageUtil.send(target, new StringBuilder("&6")
                                .append(sender.getName())
                                .append("&7")
                                .append(" has challenged you to a duel! Type \"/accept\" to accept a challenge.")
                                .toString());
                    }
                }, MESSAGE_DELAY);
                MessageUtil.send(challenger, new StringBuilder()
                        .append("&7You have challenged ")
                        .append("&6")
                        .append(challenged.getName())
                        .append("&7")
                        .append(" to a duel!")
                        .toString());
            }
        }
    }

    /**
     * Preps a player for a duel.
     *
     * @param player the prepared player
     * @param target the other player of the duel
     * @param teleport teleports player to targeted player if true
     */
    private void preparePlayer(Player player, Player target, boolean teleport) {
        for (Player other : Bukkit.getOnlinePlayers()) {
            if (!other.getUniqueId().equals(target.getUniqueId())) {
                player.hidePlayer(other);
            }
        }

        player.setHealth(20L);
        player.setExhaustion(20L);

        if (teleport) {
            player.teleport(target);
        }
    }

    /**
     * Resets the player state.
     *
     * @param player the player
     */
    private void resetPlayer(Player player) {
        for (Player other : Bukkit.getOnlinePlayers()) {
            player.showPlayer(other);
        }

        player.setHealth(20L);
        player.setExhaustion(20L);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        final UUID uuid = event.getPlayer().getUniqueId();
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
            List<DuelState> remove = this.states.stream().filter(state -> state.getChallenger().equals(uuid)
                    || state.getChallenged().equals(uuid)).collect(Collectors.toList());
            this.states.removeAll(remove);
        });
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Player target = (Player) event.getEntity();
            Optional<DuelState> optionalState = findActiveDuel(target.getUniqueId());
            if (optionalState.isPresent()) {
                DuelState state = optionalState.get();
                if (event.getDamager() instanceof Player) {
                    Player damager = (Player) event.getDamager();
                    if (!(state.getChallenger().equals(damager.getUniqueId())
                            || state.getChallenged().equals(damager.getUniqueId()))) {
                        event.setCancelled(true);
                        MessageUtil.send(damager, new StringBuilder("&6")
                                .append(target.getName())
                                .append("&c")
                                .append(" is in a duel and cannot be attacked.")
                                .toString());
                    } else if (!state.isStarted()) {
                        event.setCancelled(true);
                    }
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() instanceof Player && isDueling(event.getTarget().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Optional<DuelState> optionalState = findActiveDuel(player.getUniqueId());
        if (optionalState.isPresent()) {
            DuelState state = optionalState.get();
            Optional<Player> optionalWinner = PlayerUtil.find(player.getUniqueId().equals(state.getChallenged())
                    ? state.getChallenger()
                    : state.getChallenged());
            if (optionalWinner.isPresent()) {
                Player winner = optionalWinner.get();
                MessageUtil.send(winner, new StringBuilder("&6")
                        .append("You won the duel!")
                        .toString());
                resetPlayer(winner);
            }
            MessageUtil.send(player, new StringBuilder("&6")
                    .append("Your opponent won the duel!")
                    .toString());
            resetPlayer(player);
        }
    }

}
