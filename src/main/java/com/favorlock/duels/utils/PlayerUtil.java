package com.favorlock.duels.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

public final class PlayerUtil {

    private PlayerUtil() {
    }

    public static Optional<Player> find(String name) {
        Player target = null;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getName().equalsIgnoreCase(name)) {
                target = player;
                break;
            }
        }
        return Optional.ofNullable(target);
    }

    public static Optional<Player> find(UUID uuid) {
        Player target = null;
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getUniqueId().equals(uuid)) {
                target = player;
                break;
            }
        }
        return Optional.ofNullable(target);
    }

}
