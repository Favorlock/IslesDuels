package com.favorlock.duels;

import com.favorlock.duels.commands.AcceptCommand;
import com.favorlock.duels.commands.DuelCommand;
import com.favorlock.duels.system.DuelManager;
import com.favorlock.duels.tasks.MovementTracker;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private DuelManager duelManager;
    private MovementTracker movementTracker = new MovementTracker();

    @Override
    public void onEnable() {
        initCommands();
        initManagers();

        Bukkit.getScheduler().runTaskTimer(this, this.movementTracker, 0, MovementTracker.THRESHOLD);
        Bukkit.getPluginManager().registerEvents(this.movementTracker, this);
    }

    private void initCommands() {
        getCommand("duel").setExecutor(new DuelCommand(this));
        getCommand("accept").setExecutor(new AcceptCommand(this));
    }

    private void initManagers() {
        Bukkit.getPluginManager().registerEvents(this.duelManager = new DuelManager(this), this);
    }

    public DuelManager getDuelManager() {
        return this.duelManager;
    }

    public MovementTracker getMovementTracker() {
        return movementTracker;
    }
}
