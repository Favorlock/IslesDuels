package com.favorlock.duels.commands;

import com.favorlock.duels.Main;
import com.favorlock.duels.utils.MessageUtil;
import com.favorlock.duels.utils.PlayerUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Command players execute to initiate a duel.
 */
public class DuelCommand implements CommandExecutor {

    private Main plugin;

    public DuelCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean result = false;
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                result = true;
                String name = args[0];
                if (!name.equalsIgnoreCase(sender.getName())) {
                    Optional<Player> optionalTarget = PlayerUtil.find(name);
                    if (optionalTarget.isPresent()) {
                        Player target = optionalTarget.get();
                        this.plugin.getDuelManager().invite(player, target);
                    } else {
                        MessageUtil.send(sender, new StringBuilder("&6")
                                .append(name)
                                .append("&c")
                                .append(" is not online.")
                                .toString());
                    }
                } else {
                    MessageUtil.send(sender, new StringBuilder("&c")
                            .append("You cannot challenge yourself to a duel.")
                            .toString());
                }
            }
        } else {
            result = true;
            MessageUtil.send(sender, new StringBuilder("&c")
                    .append("Only players can initiate duels.")
                    .toString());
        }
        return result;
    }

}
