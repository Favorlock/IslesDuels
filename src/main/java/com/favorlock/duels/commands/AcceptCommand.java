package com.favorlock.duels.commands;

import com.favorlock.duels.Main;
import com.favorlock.duels.utils.MessageUtil;
import com.favorlock.duels.utils.PlayerUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * Command that players execute to accept a duel.
 */
public class AcceptCommand implements CommandExecutor {

    private Main plugin;

    public AcceptCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            this.plugin.getDuelManager().accept(player);
        } else {
            MessageUtil.send(sender, new StringBuilder("&c")
                    .append("Only players can accept duels.")
                    .toString());
        }
        return true;
    }

}
